from telethon.sync import TelegramClient

api_id = "your_api_id"
api_hash = 'your_api_hash'
username = 'some_username'
client = TelegramClient(username, api_id, api_hash)

# collect messages and date
messages = []
messages_date = []

# get last 30000 msg for example
# 'analyst_hunter' - telegram's channel name
with client:
    for msg in client.iter_messages('analysts_hunter', 30000):
        if isinstance(msg.text, str) and '#вакансия' in msg.text and 'Требования:' in msg.text:
            messages.append(msg.text)
            messages_date.append(msg.date.strftime('%d.%m.%Y'))


# save it in txt file
f = open('f_text_1.txt', 'w', encoding='utf-8')
for el in messages:
    f.write(el)
f.close()

f2 = open('f_data_1.txt', 'w', encoding='utf-8')
for el in messages_date:
    f2.write(str(el)+'\n')
f2.close()
